import RadioboxVariant from './js/RadioboxVariant'

// initialize the RadioboxVariants objects
var radioboxVariants = document.getElementsByClassName('js-radiobox-variant')
if (radioboxVariants.length > 0) {
  for (var i = 0; i < radioboxVariants.length; i++) {
    new RadioboxVariant(radioboxVariants[i])
  }
}

export default class RadioboxVariant {
  constructor(element) {
    this.element = element
    this.inputs = this.element.querySelectorAll("[name='radiobox-variant']")

    this.initEvent()
  }

  initEvent() {
    for (const input of this.inputs) {
      input.addEventListener('click', this.scrollTo)
    }
  }

  scrollTo(e) {
    const inputId = e.target.id
    const offsetTop = document.querySelector(`#product-${inputId}`).offsetTop

    scroll({
      top: offsetTop,
      behavior: 'smooth'
    })
  }
}
